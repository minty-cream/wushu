//MUTATIONS

module.exports.UPDATE_MESSAGES = "update_messages"
module.exports.UPDATE_CHANNELS = "update_channels"

//Entity Types

module.exports.CHARACTER="character"
module.exports.CHANNEL="channel"
module.exports.ROOM="room"
module.exports.STORY="story"
module.exports.INFORMATION="information"
