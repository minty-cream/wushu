const uuid = require('uuid/v4')
const _ = require('lodash')
const consts=require('./consts')


module.exports=(db)=>({
	get_channels:async (_dbref)=>{
		let __channels = await db.all("SELECT * FROM channels WHERE dbref IN (SELECT channel_dbref FROM subscriptions WHERE user_dbref=?)",_dbref)
		return __channels
	},
	get_value:async (_dbref, _attribute)=>{
		let row=await db.get("SELECT * FROM eav WHERE dbref=? AND attribute=?",[ _dbref, _attribute])
		return row
	},
	get_entities:async(value, attribute)=>{
		let _vars = value.concat(attribute)
		let _value = Array(value.length).fill("?").join()
		let _attribute = Array(attribute.length).fill("?").join()
		let _selector = "SELECT * FROM eav WHERE value IN ("+_value +") AND attribute IN ("+_attribute+")"
		let row=await db.all(_selector, _vars )
		return row
	},
	get_subscribers:async(_channel)=>{
		let _selector = "SELECT user_dbref FROM subscriptions WHERE channel_dbref=?"
		let row=await db.all(_selector, _channel )
		return row
	},
	add_channel:async(_payload,_conn)=>{
		let _statement="INSERT INTO channels(dbref,user_dbref,name) VALUES(?,?,?)"
		let __statement="INSERT INTO subscriptions(channel_dbref,user_dbref) VALUES(?,?)"
		let _uuid=uuid()
		await db.run(_statement,[_uuid,_conn.dbref,_payload.name])
		if(_payload.keep_log){
			await db.run('UPDATE channels SET keep_log=1 WHERE dbref=?',_uuid)
		}
		if(_payload.private < 1){
			/*
			await Promise.all(_destination_list.reduce((_promises,_destination)=>{
				let _db=db.run(__statement,[_uuid,_destination.dbref])
				_promises.push(_db)
			},[]))
			*/
		}else{
			await db.run('UPDATE channels SET is_public=1 WHERE dbref=?',_uuid)
		}
		return await db.get('SELECT * FROM channels WHERE dbref=?',[_uuid])
	},
	edit_channel:async(_channel_id,_conn)=>{
		return []
	},
	delete_channel:async(_channel_id,_conn)=>{
		let _statement="DELETE FROM channels WHERE dbref=? and user_dbref=?"
		await db.run(_statement,[_channel_id,_conn.dbref])
	},
	get_user:async(_user,_pass)=>{
		let _statement="SELECT username,dbref FROM users WHERE username=? AND password=?"
		let _row = await db.get(_statement,[_user,_pass])
		return _row
	},
	add_user:async(_user,_pass)=>{
		let _statement="INSERT INTO users VALUES(?,?,?)"
		let _id = uuid()
		await db.run(_statement,[_id,_user,_pass])
	},
	edit_user:async(_user,_pass)=>{
		return []
	},
	delete_user:async(_user,_pass)=>{
		return []
	},
	add_character:async(_user,_character)=>{
		return []
	},
	edit_character:async(_user,_character)=>{
		return []
	},
	delete_character:async(_user,_character)=>{
		return []
	},
	add_post:async(_user,_post)=>{
		return []
	},
	edit_post:async(_user,_post)=>{
		return []
	},
	delete_post:async(_user,_post)=>{
		return []
	}
})
