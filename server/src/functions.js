const _ = require('lodash')
const consts=require('./consts')

module.exports=(db)=>{
	const mutations = require('./mutations')(db)
	const _export={}
	_export.connect=(sock)=>{
		return mutations.connect(sock)
	}
	_export.close=(conn)=>{
	}
	_export.process=async (_conn, _message)=>{
		/**
		 * {
		 * 	"mutation": /anything from the mutations object/
		 * 	"payload": /whatever the action needs to function/
		 * }
		 **/
		await mutations.mutate(_message,_conn)
	}
	return _export
}
