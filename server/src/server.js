const WebSocket = require('ws')
const sqlite = require('sqlite')

const start=async (DB_LOCATION,WEB_PORT)=>{
	return await Promise.resolve()
		.then(() => sqlite.open(DB_LOCATION, { Promise }))
		.then(db => db.migrate({force: 'last'}))
		.then(db => require('./functions')(db))
		.then($=>{
			const ws = new WebSocket.Server({port:WEB_PORT})
			ws.on('connection',(sock)=>{
				const _conn = $.connect(sock)
				sock.on('message', (_message)=>{ $.process(_conn, _message) })
				sock.on('close', ()=>{ $.close(_conn) })
			})
			return ws
		})
}

module.exports = start
