const _ = require('lodash')
const consts=require('./consts')
const uuid=require('uuid/v4')

module.exports=(db)=>{
	const $ = require('./controllers')(db)

	const conns={}
	const users={}

	const mutations={}
	const createMutation=(controller)=>{
		//TODO if you keep this make it a whitelist type thing
		return async (_payload,_conn)=>{
			let __payload = await $[controller](_payload,_conn)
			return [
				{
					"mutation":controller.toUpperCase()+"_SUCCESFUL",
					"payload": __payload
				}
			]
		}
	}
	for(controller in $){
		mutations[controller]=createMutation(controller)
	}

	mutations.SEND_MESSAGE=async (_payload, _conn)=>{
		//A message might start with ; : \
		//If it doesn't, its a say string ... otherwise
		// ";":"semipose" Namemessage
		// ":":"pose" Name message
		// "\":"emit" Name> message
		// nothing Name says, "message"
		let _output = {
			"mutation": "UPDATE_MESSAGES",
			"payload":{}
		}
		let __destination_list=await $.get_subscribers(_payload.to)
		_output.payload.to=_payload.to
		_output.payload.from = _conn.username
		_output.payload.message = _payload.message
		_.forEach(__destination_list,(_user)=>{
			if(users[_user.user_dbref]){
				_send([_output],users[_user.user_dbref])
			}
		})
	}

	mutations.login=async (_payload,_conn)=>{
		if(conns[_conn.id].dbref){
			throw "Already logged in."
		}
		let _user = await $.get_user(_payload.username,_payload.password)
		_conn.dbref = _user.dbref
		_conn.username = _user.username
		conns[_conn.id]=_conn
		users[_user.dbref]=_conn
		let __channels = await $.get_channels(_conn.dbref)
		return [
			{
				"mutation":"INITIATE_CHANNELS",
				"payload":{
					"channels":__channels
				}
			},
			{
				"mutation":"LOGIN_SUCCESS",
				"payload":{
					"logged_in": true,
					"user": _user
				}
			}
		]
	}


	mutations.signup=async (_payload,_conn)=>{
		if(conns[_conn.id].dbref){
			throw "Already logged in."
		}
		await $.add_user(_payload.username,_payload.password)
		return await mutations.login(_payload, _conn)
	}

	const _send=(_outputs, _conn)=>{
		_.forEach(_outputs,(_output)=>{
			let __output=JSON.stringify(_output)
			_conn.socket.send(__output)
		})
	}

	const _export={}
	_export.connect=(sock)=>{
		let _id=uuid()
		let _conn={"id":_id,"socket":sock}
		conns[_id]=_conn
		return _conn
	}

	_export.close=()=>{
		if(conns[conn.id].dbref){
			delete users[conns[conn.id].dbref]
		}
		delete conns[conn.id]
	}

	_export.mutate=async (_message, _conn)=>{
		try{
			let _data=JSON.parse(_message)
			let _mutation=_data.mutation
			let _payload=_data.payload
			console.log(_data)
			if(conns[_conn.id].dbref){
				_conn = users[conns[_conn.id].dbref]
				if(mutations[_mutation]){
					let _outputs = await mutations[_mutation](_payload, _conn)
					_send(_outputs, _conn)
				}else{
					throw "Unknown Mutation "+_mutation
				}
			}else if(
				_mutation==="login" ||
				_mutation==="signup"
			){
				let _outputs = await mutations[_mutation](_payload, _conn)
				_send(_outputs, _conn)
			}else{
				throw "Only unauthenticated options are login and signup"
			}
		}catch(error){
			_send([{
				"mutation":"error",
				"payload":error.toString()
			}],_conn)
		}
	}
	return _export
}
