const net = require('net')
const PORT = 6969
net.createServer((sock)=>{
	let _id=uuid()
	let _conn={"id":_id,"type":"socket","socket":sock}
	conns[_id]=_conn
	sock.on('data', (_message)=>{ process(conns[_id], _message) })
	sock.on('close', ()=>{ delete conns[_id] })
	notify(UPDATE_MESSAGES,_welcome(),_conn)
}).listen(PORT)
console.log('Server listening on ' + PORT)
const process=async (_conn, _message)=>{
	_message=_message.toString().trim()
	const db = await dbPromise
	if(_conn.dbref){
		//@addcom[/story] general/user1 user2=message
		if(_message.toLowerCase().startsWith('@addcom')){
			let __data=_message.split("=",1)
			let __message=__data[1]
			__data = __data[0].split(" ",1)
			let __flags = __data[0].split("/")
			__data = __data[1].split("/")
			let __channel = __data[0]
			let __log = false
			if(__flags[1] == "story"){
				__log = true
			}
			if(__data[1]){
				let __users=__data[1].split(" ")
				__destination_list=await get_entities(__users,['name'])
			}
			try{
				await add_channel(__channel,_conn,__destination_list,__log)
				if(__message){
					notify(UPDATE_MESSAGES,__message,_conn,__channel)
				}
			}catch(error){
				notify(UPDATE_MESSAGES,error,_conn)
			}
		}
		//@chat general=message
		if(_message.toLowerCase().startsWith('@chat')){
			let __data=_message.replace("@chat ","").split("=",1)
			let __channel=__data[0]
			let __message=__data[1]
			try{
				notify(UPDATE_MESSAGES,__message,_conn,__channel)
			}catch(error){
				notify(UPDATE_MESSAGES,error,_conn)
			}
		}
	}else{
		if(_message.startsWith('co')){
			let _character=[]
			if(_message.includes('"')){
				_character=_message.split('"')
			}else{
				_character=_message.split(' ')
			}
			if(_character.length===3){
				try{
					let _username=_character[1]
					let _password=_character[2]
					let user = await db.get('SELECT * FROM users WHERE username=? AND password=?',[ _username, _password ])
					if(user){
						login(user,_conn)
					}else{
						throw "Username not found or password is incorrect."
					}
				}catch(error){
					notify(UPDATE_MESSAGES,error,_conn)
				}
			}else{
				notify(UPDATE_MESSAGES,"Incorrect usage. connect <username> <password>",_conn)
			}
		}
		if(_message.startsWith('cr')){
			let _character=[]
			if(_message.includes('"')){
				_character=_message.split('"')
			}else{
				_character=_message.split(' ')
			}
			if(_character.length===3){
				let _username=_character[1]
				let _password=_character[2]
				let _id=uuid()
				try{
					await db.run('INSERT INTO users VALUES(?,?,?)',[ _id, _username, _password ])
					notify(UPDATE_MESSAGES,"Character created.",_conn)
					login({
						"dbref": _id,
						"username": _username
					},_conn)
				}catch(error){
					notify(UPDATE_MESSAGES,error,_conn)
				}
			}else{
				notify(UPDATE_MESSAGES,"Incorrect usage. create <username> <password>",_conn)
			}
		}
	}
}
//This is in this file to keep the comments.
//The JSON version won't need to worry about this?
const mutations={}
mutations[UPDATE_MESSAGES]=(_output, _conn)=>{
	//A message might start with ; : \
	//If it doesn't, its a say string ... otherwise
	// ";":"semipose" Namemessage
	// ":":"pose" Name message
	// "\":"emit" Name> message
	// nothing Name says, "message"
	_output.payload.socket_message=_output.payload.message
	_output.payload.echo_socket_message=_output.payload.message
	return _output
}
		if(__conn.type=="socket"){
			__conn.socket.write(_output.payload.socket_message+' \r\n')
		}else{
