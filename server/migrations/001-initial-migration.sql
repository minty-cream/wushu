-- Up
-- PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS users
(
	dbref BLOB UNIQUE NOT NULL,
	username TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS characters
(
	dbref BLOB UNIQUE NOT NULL,
	user_dbref BLOB NOT NULL,
	name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS entities
(
	-- Wiki articles, mostly. They might incorporate rooms. They might just be an object.
	dbref BLOB UNIQUE NOT NULL,
	user_dbref BLOB NOT NULL,
	name TEXT NOT NULL,
	kind TEXT NOT NULL DEFAULT "entity"
	-- Whether or not this entity is a ROOM or an OBJECT. Honestly not *too* important since this all turns into wiki articles.
	-- Seems useful enough to be its own thing seperate from the eav. Traditionally "type" in MUSHes.
);

CREATE TABLE IF NOT EXISTS eav
(
	dbref BLOB NOT NULL,
	-- This dbref references something else. Usually a character
	attribute TEXT NOT NULL,
	-- If you have sub attributes, just do meta tagging. stat:strength, stat:dexterity, etc
	value TEXT NOT NULL CHECK (length(value) < 2666667)
	-- I assume it'll take all of five seconds for someone to upload a picture to this db using base64. Heres some protection.
	-- Roughly 2 Megabytes. 8-bit to 6-bit conversion is hard and I write this entire thing on sleeping pills.
);

CREATE TABLE IF NOT EXISTS posts
(
	dbref BLOB UNIQUE NOT NULL,
	user_dbref BLOB NOT NULL,
	-- user_dbref can be a user or a character, useful for puppeting or ic channels
	channel_dbref BLOB NOT NULL,
	position INTEGER NOT NULL DEFAULT -1,
	value TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS channels
(
	dbref BLOB UNIQUE NOT NULL,
	user_dbref BLOB NOT NULL,
	-- user_dbref can be a user or a character, useful for puppeting or ic channels
	name TEXT UNIQUE NOT NULL,
	description TEXT NOT NULL DEFAULT "No description.",
	is_public INTEGER NOT NULL DEFAULT 0,
	-- 0 requires invite
	-- 1 appears in the public list
	-- join_id UNIQUE TEXT, -- Future update
	-- If set, invitations are possible
	keep_log INTEGER NOT NULL DEFAULT 0
	-- 0 don't log to database
	-- 1 log to database
);

CREATE TABLE IF NOT EXISTS subscriptions
(
	channel_dbref BLOB NOT NULL,
	user_dbref BLOB NOT NULL
	-- user_dbref can also be a character, useful for puppeting or ic channels
);

INSERT INTO users VALUES (0,"admin","password");
INSERT INTO channels(dbref,user_dbref,name) VALUES(1,0,"general");

CREATE TRIGGER add_subscription_to_general
AFTER INSERT ON users
BEGIN
	INSERT INTO subscriptions VALUES(1,NEW.dbref);
END;

CREATE TRIGGER increment_story
AFTER INSERT ON posts
WHEN NEW.story_position IS -1 BEGIN
	UPDATE posts SET position=(SELECT MAX(position)+1 FROM posts WHERE channel_dbref=NEW.channel_dbref) WHERE rowid=NEW.rowid;
END;

-- Down

DROP TRIGGER add_subscription_to_general;
DROP TRIGGER increment_story;
DROP TABLE users;
DROP TABLE characters;
DROP TABLE entities;
DROP TABLE eav;
DROP TABLE posts;
DROP TABLE channels;
DROP TABLE subscriptions;
