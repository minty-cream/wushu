const server = require('./src/server')
const WEB_PORT = 6970
const DB_LOCATION = './database.sqlite'
const start=async ()=>{
	let ws = await server(DB_LOCATION,WEB_PORT)
	console.log('WebSocket Server started')
	console.log(ws.address())
}
start()
