const assert = require('assert')
const sqlite = require('sqlite')
const start = require('../src/server')
const WebSocket = require('ws')
let server
let ws
let state = {}

beforeEach('create websocket',async function(){
	server = await start(':memory:',0)
	let _address = server.address()
	_address = "ws://localhost:"+_address.port
	ws = new WebSocket(_address)
})

afterEach('stop server',function(){
	server.close()
})

describe('#mutate()',function(){
	it('should refuse anything that is not json',function(done){
		ws.on('open',()=>{
			ws.on('message',(event)=>{
				let _result = JSON.parse(event)
				let _expected = {
					"mutation":"error",
					"payload":"SyntaxError: Unexpected token e in JSON at position 1"
				}
				assert.deepStrictEqual(_result, _expected)
				done()
			})
			ws.send("test")
		})
	})
	it('should refuse an unknown command',function(done){
		ws.on('open',()=>{
			ws.on('message',(event)=>{
				let _result = JSON.parse(event)
				let _expected = {
					"mutation":"error",
					"payload":"Only unauthenticated options are login and signup"
				}
				assert.deepStrictEqual(_result, _expected)
				done()
			})
			ws.send(JSON.stringify({
				"mutation":"test"
			}))
		})
	})
})

describe('#signup()',function(){
	it('should sign you up',function(done){
		let _expected = {
			"mutation":"REGISTRATION_SUCCESS",
			"payload":{
				"registered":true
			}
		}
		ws.on('message',(event)=>{
			let _result = JSON.parse(event)
			if(_result.mutation == "REGISTRATION_SUCCESS"){
				assert.deepStrictEqual(_result, _expected)
				done()
			}
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({
				"mutation":"signup",
				"payload":{
					"username":"user",
					"password":"pass"
				}
			}))
		})
	})
})

describe('#login()',function(){
	it('should log you in',function(done){
		let _expected = {
			"LOGIN_SUCCESS":
			{
				"mutation":"LOGIN_SUCCESS",
				"payload":{
					"logged_in":true
				}
			},
			"UPDATE_CHANNELS":
			{
				"mutation":"UPDATE_CHANNELS",
				"payload":{
					"channels":[
						{
							"dbref":1,
							"user_dbref":0,
							"name":"general",
							"description":"No description.",
							"is_public":0,
							"keep_log":0
						}
					]
				}
			}
		}

		let _results = {}
		ws.on('message',(event)=>{
			let _result = JSON.parse(event)
			if(_result.mutation == "REGISTRATION_SUCCESS"){
				ws.send(JSON.stringify({ "mutation":"login", "payload":{ "username":"username", "password":"password" } }))
			}
			if(_expected[_result.mutation]){
				_results[_result.mutation]=_result
			}
			if(Object.keys(_results).length > 1){
				for(mutation in _expected){
					assert.deepStrictEqual(_results[mutation],_expected[mutation])
				}
				done()
			}
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({
				"mutation":"signup",
				"payload":{
					"username":"username",
					"password":"password"
				}
			}))
		})
	})
})

const process=(event, expected, results, done, action)=>{
	let _result = JSON.parse(event)
	if(_result.mutation == "REGISTRATION_SUCCESS"){
		ws.send(JSON.stringify({ "mutation":"login", "payload":{ "username":"username", "password":"password" } }))
	}
	if(_result.mutation == "LOGIN_SUCCESS"){
		action()
	}
	if(expected[_result.mutation]){
		for(key in _result.payload){
			if(key.includes('dbref')){
				expected[_result.mutation].payload[key]=_result.payload[key]
			}
		}
		results[_result.mutation]=_result
	}
	if(Object.keys(results).length >= Object.keys(expected).length){
		for(mutation in expected){
			assert.deepStrictEqual(results[mutation],expected[mutation])
		}
		done()
	}
	return results
}

describe('#message()',function(){
	it('should send a message',function(done){
		let _expected = {
			"SENT_SUCCESFUL":
			{
				"mutation": "SENT_SUCCESFUL",
				"payload": {
					"sent":true
				}
			},
			"UPDATE_MESSAGES":
			{
				"mutation":"UPDATE_MESSAGES",
				"payload":{
					"to":"general",
					"from":"username",
					"message":"words"
				}
			}
		}

		let _results = {}
		ws.on('message',(event)=>{
			_results = process(event, _expected, _results, done, ()=>{
				ws.send(JSON.stringify({
					"mutation":"send_message",
					"payload":{
						"to":"general",
						"message":"words"
					}
				}))
			})
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({ "mutation":"signup", "payload":{ "username":"username", "password":"password" } }))
		})
	})
})

describe('#add_channel()',function(){
	it('should add a public channel',function(done){
		let _expected = {
			"ADD_CHANNEL_SUCCESFUL":
			{
				"mutation": "ADD_CHANNEL_SUCCESFUL",
				"payload":{
					"name":"custom",
					"description": "No description.",
					"keep_log": 0,
					"is_public": 0
				}
			}
		}

		let _results = {}
		ws.on('message',(event)=>{
			_results = process(event, _expected, _results, done, ()=>{
				ws.send(JSON.stringify({
					"mutation":"add_channel",
					"payload":{
						"name":"custom",
						"private":false
					}
				}))
			})
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({ "mutation":"signup", "payload":{ "username":"username", "password":"password" } }))
		})
	})
	it('should add a private channel',function(done){
		let _expected = {
			"ADD_CHANNEL_SUCCESFUL":
			{
				"mutation": "ADD_CHANNEL_SUCCESFUL",
				"payload":{
					"name":"custom",
					"description": "No description.",
					"keep_log": 0,
					"is_public": 1
				}
			}
		}

		let _results = {}
		ws.on('message',(event)=>{
			_results = process(event, _expected, _results, done, ()=>{
				ws.send(JSON.stringify({
					"mutation":"add_channel",
					"payload":{
						"name":"custom",
						"private":true
					}
				}))
			})
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({ "mutation":"signup", "payload":{ "username":"username", "password":"password" } }))
		})
	})
	it('should add a logging public channel',function(done){
		let _expected = {
			"ADD_CHANNEL_SUCCESFUL":
			{
				"mutation": "ADD_CHANNEL_SUCCESFUL",
				"payload":{
					"name":"custom",
					"description": "No description.",
					"keep_log": 1,
					"is_public": 0
				}
			}
		}

		let _results = {}
		ws.on('message',(event)=>{
			_results = process(event, _expected, _results, done, ()=>{
				ws.send(JSON.stringify({
					"mutation":"add_channel",
					"payload":{
						"name":"custom",
						"keep_log": true,
						"private":false
					}
				}))
			})
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({ "mutation":"signup", "payload":{ "username":"username", "password":"password" } }))
		})
	})
	it('should add a logging private channel',function(done){
		let _expected = {
			"ADD_CHANNEL_SUCCESFUL":
			{
				"mutation": "ADD_CHANNEL_SUCCESFUL",
				"payload":{
					"name":"custom",
					 "description": "No description.",
					"keep_log": 1,
					"is_public": 1
				}
			}
		}

		let _results = {}
		ws.on('message',(event)=>{
			_results = process(event, _expected, _results, done, ()=>{
				ws.send(JSON.stringify({
					"mutation":"add_channel",
					"payload":{
						"name":"custom",
						"keep_log": true,
						"private":true
					}
				}))
			})
		})

		ws.on('open',()=>{
			ws.send(JSON.stringify({ "mutation":"signup", "payload":{ "username":"username", "password":"password" } }))
		})
	})
})
