import Vue from 'vue'
import Vuex from 'vuex'
import AnsiUp from 'ansi_up'
import uuid from 'uuid/v4'
import router from './router'
Vue.use(Vuex)

const createWebSocketPlugin = (socket) => {
	return store=>{
		socket.onmessage=event=>store.dispatch('onmessage',event)
		store.subscribe(mutation=>{
			if(mutation.type == 'send') socket.send(JSON.stringify(mutation.payload))
		})
	}
}

const _ws = new WebSocket('ws://localhost:6970')
const socket_plugin = createWebSocketPlugin(_ws)

const createStore = () => {
	return new Vuex.Store({
		state: {
			display: {}, //The actual shit thats on screen
			channels: {}, //Just a list of the available channels for the sidebar
			messages: {}, //All the messages for individual channels and crap
			user: null,
			logged_in: false,
			received: false
		},
		mutations: {
			LOGIN_SUCCESS (state, _payload){
				state.logged_in = true
				state.user = _payload.user
				router.push({name:'Chat'})
			},
			UPDATE_MESSAGES (state,_payload){
				let _dbref = _payload.dbref
				state.received=true
				if(state.messages[_dbref]){
					state.messages[_dbref] = state.messages[_dbref].concat([_payload])
				}else{
					state.messages[_dbref] = [_payload]
				}
				if(state.display.channel === _payload.to){
					state.display.messages = state.messages[_dbref]
				}
			},
			INITIATE_CHANNELS (state, _payload){
				state.channels= _payload.channels
				state.display = {
					channel: _payload.channels[0].dbref,
					messages: []
				}
			},
			UPDATE_CHANNELS (state,_payload){
				state.channels= _payload.channels
			},
			change_channel (state, dbref){
				if(!state.messages[dbref]){
					state.messages[dbref]=[]
				}
				state.display = {
					channel: dbref,
					messages: state.messages[dbref]
				}
			},
			send (state, _message){
				//This is just a dummy function for the sake of the plugin
			},
			received (state, _received){
				state.received = _received
			}
		},
		actions: {
			onmessage ({commit,state}, _event){
				try{
					let _data = JSON.parse(_event.data)
					console.log(_data)
					commit(_data.mutation,_data.payload)
				}catch(error){
					console.error(error);
				}
			},
			login({commit, state}, _payload){
				commit('send',{
					"mutation":"login",
					"payload": _payload
				})
			},
			signup({commit, state}, _payload){
				commit('send',{
					"mutation":"signup",
					"payload":_payload
				})
			},
			send_message({commit, state}, _payload){
				commit('send',{
					"mutation":"SEND_MESSAGE",
					"payload": {
						"to": state.display.channel,
						"message": _payload
					}
				})
			}
		},
		plugins: [socket_plugin]
	})
}

export default createStore
