import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages'
import ChatIndex from '@/pages/chat'
import Chat from '@/pages/chat/chat'
import Wiki from '@/pages/wiki'
import AddCharacter from '@/pages/characters/add'
import AddUser from '@/pages/users/add'
import AddChannel from '@/pages/channels/add'
import EditCharacter from '@/pages/characters/edit'

Vue.use(Router)

export default new Router({
	routes: [
		{ path: '/', name: 'Home', component: Home },
		{ path: '/chat', component: ChatIndex, children: [
			{ path: '', name: 'Chat', component: Chat},
			{ path: 'channel/add', name: 'Add Channel', component: AddChannel}
		] },
		{ path: '/wiki', name: 'Wiki', component: Wiki },
		{ path: '/signup', name: 'Sign Up', component: AddUser },
		{ path: '/character/add', name: 'Add Character', component: AddCharacter},
		{ path: '/character/edit', name: 'EditCharacter', component: EditCharacter},
	]
})
